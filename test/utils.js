const {ethers} = require("hardhat");

const initStore = async () => {
    const StoreContract = await ethers.getContractFactory("Store");
    const store = await StoreContract.deploy();
    await store.deployed();
    [owner, client1] = await ethers.getSigners();

    return [store, StoreContract, owner, client1];
}

module.exports = {initStore};