const {expect} = require("chai");
const {ethers} = require("hardhat");
const {initStore} = require('./../utils');

describe("Store Contract - Owner", async () => {
    let store, StoreContract, owner, client1;

    beforeEach(async () => {
        [store, StoreContract, owner, client1] = await initStore();
    });

    it("Should save the correct owner address", async () => {
        expect(store.owner).to.exist;
        let storeOwner = await store.owner();
        expect(storeOwner).to.eq(owner.address);
    });
});