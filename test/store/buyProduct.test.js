const {expect} = require("chai");
const {ethers} = require("hardhat");
const {initStore} = require('./../utils');

describe("Store Contract - buyProduct", async () => {
    let store, StoreContract, owner, client1;

    beforeEach(async () => {
        [store, StoreContract, owner, client1] = await initStore();
    });

    it("Should throw an error when buyProducts is called on a product that does not exist", async () => {
        try {
            let transaction = await store.buyProduct(1);
            await transaction.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.message).to.exist;
            expect(err.message).to.include("ProductNotFound");
        }
    });

    it("Should throw an error when buyProduct is called on a product that is not in stock", async () => {
        try {
            let transaction = await store.addProduct("Apple", 5, 10);
            await transaction.wait();

            transaction = await store.updateQuantity(0, 0);
            await transaction.wait();

            transaction = await store.buyProduct(0);
            await transaction.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.message).to.exist;
            expect(err.message).to.include("NoProductsInStock");
        }
    });

    it("Clients can buy products", async () => {
        let transaction = await store.addProduct("Apple", 5, 10);
        await transaction.wait();

        transaction = await store.buyProduct(0, {
            value: 10
        });
        await transaction.wait();

        let apple = await store.products(0);
        expect(apple).to.exist;
        expect(apple.name).to.eq("Apple");
        expect(apple.quantity).to.eq(4);
        expect(apple.price).to.eq(10);
    });

    it("Clients can buy a product only once", async () => {
        let transaction = await store.addProduct("Apple", 5, 10);
        await transaction.wait();

        transaction = await store.buyProduct(0, { value: 10 });
        await transaction.wait();

        try {
            transaction = await store.buyProduct(0, { value: 10 });
            await transaction.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.message).to.exist;
            expect(err.message).to.include("DuplicateProduct");
        }
    });

    it("Should throw an error when buyProduct is called on a non-existing product", async () => {
        try {
            let transaction = await store.buyProduct(0, { value: 10 });
            await transaction.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.message).to.exist;
            expect(err.message).to.include("ProductNotFound");
        }
    });

    it("Should throw an error when buyProduct is called with not enough funds", async () => {
        let transaction = await store.addProduct("Apple", 5, 10);
        await transaction.wait();

        try {
            transaction = await store.connect(client1).buyProduct(0, { value: 5 });
            await transaction.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.message).to.exist;
            expect(err.message).to.include("NotEnoughWei");
        }
    });
});