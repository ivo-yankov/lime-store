const {expect} = require("chai");
const {ethers} = require("hardhat");
const {initStore} = require('./../utils');

describe("Store Contract - withdraw", async () => {
    let store, StoreContract, owner, client1;

    beforeEach(async () => {
        [store, StoreContract, owner, client1] = await initStore();
    });

    it("Owner can withdraw", async () => {
        let transaction = await owner.sendTransaction({
            to: store.address,
            value: 1000,
        });
        expect(transaction).to.exist;
        expect(transaction.hash).to.exist;

        const balanceBefore = await owner.getBalance();

        transaction = await store.withdraw();
        const receipt = await transaction.wait();

        const gasUsed = receipt.gasUsed;
        const returnTxGas = transaction.gasPrice;
        const balanceAfter = await owner.getBalance();

        // before = after + gasUsed * gasPrice - returnedValue
        expect(balanceBefore).to.eq(balanceAfter.add(gasUsed.mul(returnTxGas)).sub(1000));
    });

    it("Should throw an error when a client tries to withdraw", async () => {
        try {
            let transaction = await store.connect(client1).withdraw();
            await transaction.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.message).to.exist;
            expect(err.message).to.include("NotOwner");
        }
    });

});