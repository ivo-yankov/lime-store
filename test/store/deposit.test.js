const {expect} = require("chai");
const {ethers} = require("hardhat");
const {initStore} = require('./../utils');

describe("Store Contract - deposit", async () => {
    let store, StoreContract, owner, client1;

    beforeEach(async () => {
        [store, StoreContract, owner, client1] = await initStore();
    });

    it("Should allow anyone to deposit to the contract", async () => {
        const ownerTransaction = await owner.sendTransaction({
            to: store.address,
            value: 100,
        });
        expect(ownerTransaction).to.exist;
        expect(ownerTransaction.hash).to.exist;

        const clientTransaction = await client1.sendTransaction({
            to: store.address,
            value: 100,
        });
        expect(clientTransaction).to.exist;
        expect(clientTransaction.hash).to.exist;
    });
});