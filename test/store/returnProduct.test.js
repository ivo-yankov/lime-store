const {expect} = require("chai");
const {ethers} = require("hardhat");
const {initStore} = require('./../utils');

describe("Store Contract - returnProduct", async () => {
    let store, StoreContract, owner, client1;

    beforeEach(async () => {
        [store, StoreContract, owner, client1] = await initStore();
    });

    it("Should throw an error when client tries to return a product they have not bought", async () => {
        try {
            let transaction = await store.connect(client1).returnProduct(0);
            await transaction.wait();
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.message).to.exist;
            expect(err.message).to.include("ProductNotBought");
        }
    });

    it("Clients can return products and their money gets refunded", async () => {
        let transaction = await store.addProduct("Apple", 5, 10);
        await transaction.wait();
        transaction = await store.connect(client1).buyProduct(0, { value: 10 });
        await transaction.wait();

        const balanceBefore = await client1.getBalance();

        transaction = await store.connect(client1).returnProduct(0);
        const receipt = await transaction.wait();

        const gasUsed = receipt.gasUsed;
        const returnTxGas = transaction.gasPrice;
        const balanceAfter = await client1.getBalance();

        // before = after + gasUsed * gasPrice - returnedValue
        expect(balanceBefore).to.eq(balanceAfter.add(gasUsed.mul(returnTxGas)).sub(10));
    });

    it("Should throw an error when client tries to return a product after more than 100 blocks", async () => {
        let transaction = await store.addProduct("Apple", 5, 10);
        await transaction.wait();
        transaction = await store.connect(client1).buyProduct(0, { value: 10 });
        await transaction.wait();

        // Simulate 100 blocks by buying and returning the same product 50 times as owner
        for (let i = 0; i < 50; i++) {
            transaction = await store.buyProduct(0, { value: 10 });
            await transaction.wait();
            transaction = await store.returnProduct(0);
            await transaction.wait();
        }

        try {
            transaction = await store.connect(client1).returnProduct(0);
            await transaction.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.message).to.exist;
            expect(err.message).to.include("RefundPeriodOver");
        }

    });

    it("Should throw an error when returning a product with not enough funds in contract", async () => {
        let transaction = await store.addProduct("Apple", 5, 10);
        await transaction.wait();
        transaction = await store.connect(client1).buyProduct(0, { value: 10 });
        await transaction.wait();
        transaction = await store.withdraw();
        await transaction.wait();

        try {
            transaction = await store.connect(client1).returnProduct(0);
            await transaction.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.message).to.exist;
            expect(err.message).to.include("TransferFailed");
        }
    });

});