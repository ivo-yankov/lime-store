const {expect} = require("chai");
const {ethers} = require("hardhat");
const {initStore} = require('./../utils');

describe("Store Contract - listProducts", async () => {
    let store, StoreContract, owner, client1;

    beforeEach(async () => {
        [store, StoreContract, owner, client1] = await initStore();
    });

    it("Clients can call listProducts", async () => {
        let transaction = await store.addProduct("Apple", 5, 10);
        await transaction.wait();
        transaction = await store.addProduct("Banana", 20, 150);
        await transaction.wait();
        transaction = await store.addProduct("Orange", 100, 540);
        await transaction.wait();

        let products = await store.connect(client1).listProducts();

        expect(products).to.exist;
        expect(products.length).to.eq(3);

        expect(products[0].name).to.eq("Apple");
        expect(products[0].id).to.exist;
        expect(products[0].id).to.eq(0);
        expect(products[0].quantity).to.exist;
        expect(products[0].quantity).to.eq(5);
        expect(products[0].price).to.exist;
        expect(products[0].price).to.eq(10);

        expect(products[1].name).to.eq("Banana");
        expect(products[1].id).to.exist;
        expect(products[1].id).to.eq(1);
        expect(products[1].quantity).to.exist;
        expect(products[1].quantity).to.eq(20);
        expect(products[1].price).to.exist;
        expect(products[1].price).to.eq(150);

        expect(products[2].name).to.eq("Orange");
        expect(products[2].id).to.exist;
        expect(products[2].id).to.eq(2);
        expect(products[2].quantity).to.exist;
        expect(products[2].quantity).to.eq(100);
        expect(products[2].price).to.exist;
        expect(products[2].price).to.eq(540);
    });

    it("listProducts does not return products with `quantity == 0`", async () => {
        let transaction = await store.addProduct("Apple", 5, 10);
        await transaction.wait();
        transaction = await store.addProduct("Banana", 20, 150);
        await transaction.wait();
        transaction = await store.addProduct("Orange", 100, 540);
        await transaction.wait();

        transaction = await store.updateQuantity(1, 0);
        await transaction.wait();

        let products = await store.connect(client1).listProducts();

        expect(products).to.exist;
        expect(products.length).to.eq(2);

        expect(products[0].name).to.eq("Apple");
        expect(products[0].id).to.exist;
        expect(products[0].id).to.eq(0);
        expect(products[0].quantity).to.exist;
        expect(products[0].quantity).to.eq(5);
        expect(products[0].price).to.exist;
        expect(products[0].price).to.eq(10);

        expect(products[1].name).to.eq("Orange");
        expect(products[1].id).to.exist;
        expect(products[1].id).to.eq(2);
        expect(products[1].quantity).to.exist;
        expect(products[1].quantity).to.eq(100);
        expect(products[1].price).to.exist;
        expect(products[1].price).to.eq(540);
    });


    it("Anyone can see a list of addresses that have bought a product", async () => {
        let transaction = await store.addProduct("Apple", 5, 10);
        await transaction.wait();

        transaction = await store.buyProduct(0, { value: 10 });
        await transaction.wait();

        transaction = await store.connect(client1).buyProduct(0, { value: 10 });
        await transaction.wait();

        let products = await store.connect(client1).listProducts();

        expect(products).to.exist;
        expect(products.length).to.eq(1);

        expect(products[0].name).to.eq("Apple");
        expect(products[0].id).to.exist;
        expect(products[0].id).to.eq(0);
        expect(products[0].quantity).to.exist;
        expect(products[0].quantity).to.eq(3);
        expect(products[0].price).to.exist;
        expect(products[0].price).to.eq(10);
        expect(products[0].clients).to.exist;
        expect(products[0].clients.length).to.eq(2);
        expect(products[0].clients[0]).to.eq(owner.address);
        expect(products[0].clients[1]).to.eq(client1.address);
    });
});