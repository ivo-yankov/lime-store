const {expect} = require("chai");
const {ethers} = require("hardhat");
const {initStore} = require('./../utils');

describe("Store Contract - balance", async () => {
    let store, StoreContract, owner, client1;

    beforeEach(async () => {
        [store, StoreContract, owner, client1] = await initStore();
    });

    it("Anyone can see the balance", async() => {
        let transaction = await store.addProduct("Apple", 5, 10);
        await transaction.wait();
        transaction = await store.buyProduct(0, { value: 10 });
        await transaction.wait();

        const balance1 = await store.provider.getBalance(store.address);
        expect(balance1).to.eq(10);

        const balance2 = await store.connect(client1).provider.getBalance(store.address);
        expect(balance2).to.eq(10);
    });
});