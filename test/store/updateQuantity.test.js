const {expect} = require("chai");
const {ethers} = require("hardhat");
const {initStore} = require('./../utils');

describe("Store Contract - updateQuantity", async () => {
    let store, StoreContract, owner, client1;

    beforeEach(async () => {
        [store, StoreContract, owner, client1] = await initStore();
    });

    it("Owner cannot update quantity of non-existing product", async () => {
        try {
            const transaction = await store.updateQuantity(1, 50);
            await transaction.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.message).to.exist;
            expect(err.message).to.include("ProductNotFound");
        }
    });

    it("Owner cannot update quantity to an invalid value", async () => {
        try {
            let transaction = await store.addProduct("Apple", 5, 10);
            await transaction.wait();

            transaction = await store.updateQuantity(0, -5);
            await transaction.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.code).to.exist;
            expect(err.code).to.eq("INVALID_ARGUMENT");
        }

        try {
            let transaction = await store.updateQuantity(0, "INVALID");
            await transaction.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.code).to.exist;
            expect(err.code).to.eq("INVALID_ARGUMENT");
        }
    });

    it("Non-owner cannot update product quantity", async () => {
        try {
            let transaction = await store.addProduct("Apple", 5, 10);
            await transaction.wait();

            transaction = await store.connect(client1).updateQuantity(0, 10);
            await transaction.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.message).to.exist;
            expect(err.message).to.include("NotOwner");
        }
    });

    it("Owner can update product quantity", async () => {
        let transaction = await store.addProduct("Apple", 5, 10);
        await transaction.wait();

        transaction = await store.updateQuantity(0, 10);
        await transaction.wait();

        let apple = await store.products(0);
        expect(apple).to.exist;
        expect(apple.name).to.eq("Apple");
        expect(apple.quantity).to.eq(10);
        expect(apple.price).to.eq(10);
    });

});