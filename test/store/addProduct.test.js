const {expect} = require("chai");
const {ethers} = require("hardhat");
const {initStore} = require('./../utils');

describe("Store Contract - addProduct", async () => {
    let store, StoreContract, owner, client1;

    beforeEach(async () => {
        [store, StoreContract, owner, client1] = await initStore();
    });

    it("Should throw an error when non-owner tries to add a new product", async () => {
        try {
            const addProductTx = await store.connect(client1).addProduct("Apple", 5, 10);
            await addProductTx.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.message).to.exist;
            expect(err.message).to.include("NotOwner");
        }
    });

    it("Should throw an error when owner tries to add a new product with invalid data", async () => {
        try {
            const addProductTx = await store.addProduct();
            await addProductTx.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.message).to.exist;
            expect(err.code).to.eq("MISSING_ARGUMENT");
        }

        try {
            const addProductTx = await store.addProduct("", 5, 10);
            await addProductTx.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.message).to.exist;
            expect(err.message).to.include("InvalidName");
        }

        try {
            const addProductTx = await store.addProduct("Apple", 0, 10);
            await addProductTx.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.message).to.exist;
            expect(err.message).to.include("InvalidQuantity");
        }

        try {
            const addProductTx = await store.addProduct("Apple", 5, 0);
            await addProductTx.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.message).to.exist;
            expect(err.message).to.include("InvalidPrice");
        }
    });

    it("Owner can add new products", async () => {
        const transaction = await store.addProduct("Apple", 5, 10);
        await transaction.wait();

        let apple = await store.products(0);
        expect(apple).to.exist;
        expect(apple.name).to.eq("Apple");
        expect(apple.quantity).to.eq(5);
        expect(apple.price).to.eq(10);
    });

    it("Should throw an error when owner tries to add a product with an existing name", async () => {
        {
            const transaction = await store.addProduct("Apple", 5, 10);
            await transaction.wait();
        }

        try {
            const transaction = await store.addProduct("Apple", 50, 1000);
            await transaction.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.message).to.exist;
            expect(err.message).to.include("ProductExists");
        }
    });
});