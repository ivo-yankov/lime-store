const {expect} = require("chai");
const {ethers} = require("hardhat");
const {initStore} = require('./../utils');

describe("Store Contract - updatePrice", async () => {
    let store, StoreContract, owner, client1;

    beforeEach(async () => {
        [store, StoreContract, owner, client1] = await initStore();
    });

    it("Owner cannot update price of non-existing product", async () => {
        try {
            const transaction = await store.updatePrice(1, 50);
            await transaction.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.message).to.exist;
            expect(err.message).to.include("ProductNotFound");
        }
    });


    it("Owner cannot update price to an invalid value", async () => {
        try {
            let transaction = await store.addProduct("Apple", 5, 10);
            await transaction.wait();

            transaction = await store.updatePrice(0, -5);
            await transaction.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.code).to.exist;
            expect(err.code).to.eq("INVALID_ARGUMENT");
        }

        try {
            let transaction = await store.updatePrice(0, 0);
            await transaction.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.message).to.exist;
            expect(err.message).to.include("InvalidPrice");
        }
    });

    it("Non-owner cannot update product price", async () => {
        try {
            let transaction = await store.addProduct("Apple", 5, 10);
            await transaction.wait();

            transaction = await store.connect(client1).updatePrice(0, 200);
            await transaction.wait();
            expect(true).to.eq(false);
        }
        catch(err) {
            expect(err).to.exist;
            expect(err.message).to.exist;
            expect(err.message).to.include("NotOwner");
        }
    });

    it("Owner can update product price", async () => {
        let transaction = await store.addProduct("Apple", 5, 10);
        await transaction.wait();

        transaction = await store.updatePrice(0, 200);
        await transaction.wait();

        let apple = await store.products(0);
        expect(apple).to.exist;
        expect(apple.name).to.eq("Apple");
        expect(apple.quantity).to.eq(5);
        expect(apple.price).to.eq(200);
    });
});