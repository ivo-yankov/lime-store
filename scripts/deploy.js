const hre = require("hardhat");
const {ethers} = hre;

async function main() {
    const [deployer] = await ethers.getSigners();
    console.log("Deploying contracts with the account:", deployer.address);
    console.log("Account balance:", (await deployer.getBalance()).toString());

    const StoreContract = await ethers.getContractFactory("Store");
    const store = await StoreContract.deploy();
    await store.deployed();
    console.log("Store deployed to:", store.address);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });