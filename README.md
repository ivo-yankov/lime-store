# Store Contract

Realization of the following task:

```
Using Remix develop a contract for a TechnoLime Store.
- The administrator (owner) of the store should be able to add new products and the quantity of them.
- The administrator should not be able to add the same product twice, just quantity.
- Buyers (clients) should be able to see the available products and buy them by their id.
- Buyers should be able to return products if they are not satisfied (within a certain period in blocktime: 100 blocks).
- A client cannot buy the same product more than one time.
- The clients should not be able to buy a product more times than the quantity in the store unless a product is returned or added by the administrator (owner)
- Everyone should be able to see the addresses of all clients that have ever bought a given product.

Bonus tasks:
- Use hardhat as a development environment
- Unit tests
- Make a deployment script
```

---

# Installation

- Run `npm install`
- Create an account at [alchemy](https://www.alchemy.com/). Create an app on the `ropsten` testnet. Find the API key and copy it.
- Copy `./.env-template` to `./env` and enter the API key and your private key from `metamask` for the `ropsten` network.
- To compile the contract run `npm run compile`
- To run the tests: `npm run test`
- To deploy to a local network: `npx hardhat run scripts/deploy.js`
- To deploy to Ropsten: `npx hardhat run scripts/deploy.js --network ropsten`


The contract is deployed to the Ropsten testnet at this address: [0xD0c48b786BA14A2f64D0fAef2DCf0d66f37C4D24](https://ropsten.etherscan.io/address/0xD0c48b786BA14A2f64D0fAef2DCf0d66f37C4D24)